package ua.cn.yet.waiter.service;

import ua.cn.yet.waiter.model.Item;

public interface ItemService extends GenericService<Item> {

}
